﻿
// (c.)Dakota C. Hurst - UnityTest

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class View2 : MonoBehaviour
{
	private const int kMaxCubes = 10;

	private GameObject[] cubes;

	private int totalCubeSinceStart = 0;
	private int secLastPlacedCube = -1;

	public Text timerText = null;

	private void Awake()
	{
		// DH: NOTE: not setting up search methods or procedurally generated anything, so links are required.

		if (timerText == null)
		{
			Debug.LogError("Timer Text isn't connected properly");
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#else
			Application.Quit();
#endif
		}

		cubes = new GameObject[kMaxCubes];
	}

	// DH: Timer null checked in Awake
	private void Update()
	{
		timerText.text = System.DateTime.UtcNow.Hour + " : " + System.DateTime.UtcNow.Minute + " : " + System.DateTime.UtcNow.Second;

		if (System.DateTime.UtcNow.Second != secLastPlacedCube && System.DateTime.UtcNow.Second % 2 == 0)
		{
			PlaceCube();
			secLastPlacedCube = System.DateTime.UtcNow.Second;
		}
	}

	private void PlaceCube()
	{
		if (cubes == null || cubes.Length <= 0)
		{
			return;
		}

		GameObject currentCubeGO = null;

		// DH: NOTE: Potentially create 10 up front, and enable for first 10, but not sure if desired given the task is to create them each even

		if (totalCubeSinceStart < kMaxCubes)
		{
			currentCubeGO = GameObject.CreatePrimitive(PrimitiveType.Cube);
			currentCubeGO.name += totalCubeSinceStart;
			cubes[totalCubeSinceStart] = currentCubeGO;
		}
		else
		{
			int nextCube = totalCubeSinceStart % kMaxCubes;
			currentCubeGO = cubes[nextCube];
		}

		Vector3 pos = currentCubeGO.transform.position;
		pos = new Vector3((float)totalCubeSinceStart, 0f, 0f);
		currentCubeGO.transform.position = pos;

		totalCubeSinceStart++;
	}
}
