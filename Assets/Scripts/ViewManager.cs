﻿
// (c.)Dakota C. Hurst - UnityTest

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewManager : MonoBehaviour
{
	public Canvas view1 = null;
	public Canvas view2 = null;

	private void Awake()
	{
		// DH: NOTE: not setting up search methods or procedurally generated anything, so links are required.
		
		if (view1 == null || view2 == null)
		{
			Debug.LogError("View Canvas's aren't connected properly");
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#else
			Application.Quit();
#endif
		}
	}
		
	public void GoToView2()	// DH: Linked to button in View1
	{
		if (view1.gameObject == null || view2.gameObject == null)
		{
			return;
		}

		view1.gameObject.SetActive(false);
		view2.gameObject.SetActive(true);
	}
}
